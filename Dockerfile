################## BASE IMAGE ######################

FROM biocontainers/biocontainers:latest

################## MAINTAINER ######################
MAINTAINER Mathieu Courcelles

USER root

RUN conda install -c r r  \
                       r-devtools \
                       r-htmltable && \
 #                      r-ggplot2 && \
    conda clean --all

ENV TAR="/bin/tar"
RUN Rscript -e 'devtools::install_github("gfellerlab/ggseqlogo")'

WORKDIR /home/biodocker/bin


RUN     wget https://github.com/GfellerLab/MoDec/releases/download/v1.1/MoDec-1.1.zip && \
        unzip MoDec-1.1.zip && \
        rm MoDec-1.1.zip 

RUN     sed -i 's/PATH\/TO\/R\/LIBRARIES/\/opt\/conda\/lib\/R\/library/' make_logo_report.R


# Test install
RUN ./MoDec_unix -i test/testData.txt -o test/test_out --Kmax 5 --pepLmin 12 --nruns 1 --makeReport

ENV VERSION="MoDec v1.1"
ENV PATH=$PATH:/home/biodocker/bin

ENTRYPOINT ["./MoDec_unix"]
