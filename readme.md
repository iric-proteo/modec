# MoDec container for MAPDP

This repository contains files to build a Docker container to run MoDec in MAPDP.


**Important notes**
* No Docker image is provided because of MoDec license restriction.
* Please read MoDec license before building this container.

Documentation and project license can be consulted here: 

* [MoDec](https://github.com/GfellerLab/MoDec)
* [MAPDP](https://gitlab.com/iric-proteo/mapdp)


## Build instructions

    $ git clone https://gitlab.com/iric-proteo/modec
    $ cd modec
    $ sudo docker build -t modec:build .
    
To use your Docker image in MAPDP, you must push it to your private registry:

    $ sudo docker build -t your_registry/modec:build .
    $ sudo docker push your_registry/modec:build

## MoDec usage

Use the following command to get instructions how to use MoDec.

    $ sudo docker run -it --rm -v /home/user/modec:/out modec:build

Copyright 2015-2020 Mathieu Courcelles

CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses

Pierre Thibault's lab

IRIC - Universite de Montreal